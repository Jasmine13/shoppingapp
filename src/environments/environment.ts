// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCak0LVFJTItQ98dfli_pEsKtGhA6LFcGQ",
    authDomain: "sampleshopping-c88b4.firebaseapp.com",
    projectId: "sampleshopping-c88b4",
    storageBucket: "sampleshopping-c88b4.appspot.com",
    messagingSenderId: "1087234625180",
    appId: "1:1087234625180:web:b486cb0a95b8a43ec9e759"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
