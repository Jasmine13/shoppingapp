import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-cart-modal',
  templateUrl: './cart-modal.page.html',
  styleUrls: ['./cart-modal.page.scss'],
})
export class CartModalPage implements OnInit {
  products = [];
  cart = {};
  noOfItem = 2;
  totAmnt = 0;
  constructor(private productService: ProductService,  private alertCtrl: AlertController) { }

  ngOnInit() {
    this.updateCart();
    this.productService.cart.subscribe(value => {
     this.cart = value;
   });
  }
  updateCart(){
    var cartItems = {};
    this.productService.cart.subscribe(value => {
      cartItems = value;
     });
    this.productService.getProducts().pipe().subscribe(allProducts => {
      this.products = allProducts.filter(p => cartItems[p.id]).map(product => {
        return { ...product, count: cartItems[product.id] };
      });
    });
    setTimeout(() => {
      this.products.forEach((prod) => {
        this.totAmnt = this.totAmnt + (prod["price"]*prod["count"]);
      })
    },3000)
  }
  addToCart(event, product) {
    event.stopPropagation();
    this.productService.addToCart(product.id);
    this.updateCart();
  }
  removeFromCart(event, product) {
    event.stopPropagation();
    this.productService.removeFromCart(product.id);
     this.updateCart();
  }
  viewAllCarts(){
    this.noOfItem = this.products.length;
  }
  checkout(){
    this.productService.checkoutCart();
    this.products = [];
    this.cart = {};
    alert("Order Received")
  }
}
