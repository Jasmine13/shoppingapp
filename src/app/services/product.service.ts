import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import firebase from 'firebase/app';
import { BehaviorSubject } from 'rxjs';
 
 
const CART_STORAGE_KEY = 'MY_CART';
 
const INCREMENT = firebase.firestore.FieldValue.increment(1);
const DECREMENT = firebase.firestore.FieldValue.increment(-1);
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  cart = new BehaviorSubject({});
  cartKey = null;
  productsCollection: AngularFirestoreCollection;
  constructor(private afs: AngularFirestore) { 
    this.loadCart();
    this.productsCollection = this.afs.collection('products');
  }
  getProducts() {
    return this.productsCollection.valueChanges({ idField: 'id' });
  }
  async loadCart(){
    const result = await localStorage.getItem(CART_STORAGE_KEY);
    if(result){
      this.cartKey = result;
      this.afs.collection('carts').doc(this.cartKey).valueChanges().subscribe((result: any) => {
        Object.keys(result).forEach((key) => {
          var tempKey = key;
          if(result[key] == 0){
            delete result[tempKey];
          }
        });
        delete result['lastUpdate'];
        this.cart.next(result || {});
      });
    }
    else{
      const newCart = await this.afs.collection('carts').add({
        lastUpdate: firebase.firestore.FieldValue.serverTimestamp()
      });
      this.cartKey = newCart.id;
      await localStorage.setItem(CART_STORAGE_KEY,this.cartKey);
      this.afs.collection('carts').doc(this.cartKey).valueChanges().subscribe((result: any) => {
        Object.keys(result).forEach((key) => {
          if(result[key] == 0){
            delete result[key];
          }
        });
        delete result['lastUpdate'];
        this.cart.next(result || {});
      });
    }
  }
  addToCart(id) {
    this.afs.collection('carts').doc(this.cartKey).update({
      [id]: INCREMENT,
      lastUpdate: firebase.firestore.FieldValue.serverTimestamp()
    });
   
    this.productsCollection.doc(id).update({
      stock: DECREMENT
    });
  }
   
  removeFromCart(id) {
    this.afs.collection('carts').doc(this.cartKey).update({
      [id]: DECREMENT,
      lastUpdate: firebase.firestore.FieldValue.serverTimestamp()
    });
    this.productsCollection.doc(id).update({
      stock: INCREMENT
    });
  }
   
  async checkoutCart() {
    await this.afs.collection('orders').add(this.cart.value);
   
    this.afs.collection('carts').doc(this.cartKey).set({
      lastUpdate: firebase.firestore.FieldValue.serverTimestamp()
    });
  }
}
