import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  products: Observable<any[]>;
  desserts = [] as any;
  mainCourse = [] as any;
  drinks = [] as any;
  starters = [] as any;
  cart = {};
  cartCount = 0 as any;

  constructor(private productService: ProductService) {}
   getContent() {
    return document.querySelector('ion-content');
  }
  scrollTo(elementId: string) {
    let y = document.getElementById(elementId).offsetTop+200;
    this.getContent().scrollToPoint(0,y, 4000);
}
  ngOnInit(): void {
    this.products = this.productService.getProducts();
     this.productService.cart.subscribe(value => {
      this.cartCount = Object.keys(value).length;
      this.cart = value;
    });
    this.productService.getProducts().subscribe(allProd=> {
     this.desserts = allProd.filter(p=> p.category === "dessert").map(product => {
        return product
      });
      this.mainCourse = allProd.filter(p=> p.category === "maincourse").map(product => {
        return product
      });
      this.drinks = allProd.filter(p=> p.category === "drinks").map(product => {
        return product
      });
      this.starters = allProd.filter(p=> p.category === "starters").map(product => {
        return product
      });
    });
  }
   addToCart(event, product) {
    event.stopPropagation();
    this.productService.addToCart(product.id);
    this.productService.cart.subscribe(value => {
     this.cartCount=Object.keys(value).length;
      this.cart = value;
    });
  }
   removeFromCart(event, product) {
    event.stopPropagation();
    this.productService.removeFromCart(product.id);
    this.productService.cart.subscribe(value => {
     this.cartCount=Object.keys(value).length;
      this.cart = value;
    });
  }
}
